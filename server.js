const PORT = 8000;
const express = require("express");
const cors = require("cors");
const axios = require("axios");
require("dotenv").config();

//Use express to create an instance of an express server
const app = express();
app.use(cors());

//if visiting localhost:8000, use response to send back a string
app.get("/news", (req, res) => {
  const options = {
    method: "get",
    url: "https://newsapi.org/v2/everything",
    params: {
      sources:
        "bbc-news,the-verge,wall-street-journal,cnn,independent,usa-today,associated-press,the-washington-post ",
      q: "coronavirus",
      q: "covid-19",
      q: "covid-19-cases",
      q: "COVID-19",
      language: "en",
      //search for articles 29 days from now
      from: new Date(Date.now() - 1000 * 60 * 60 * 24 * 29).toISOString(),
    },

    headers: {
      "X-Api-Key": process.env.REACT_APP_NEWS_API_KEY,
    },
  };
  axios
    .request(options)
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      console.log(error);
    });
});

app.listen(8000, () => console.log(`Listening on port ${PORT}`));
