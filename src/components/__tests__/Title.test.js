import {render, screen, cleanup} from '@testing-library/react';
import Title from "../header/Title";
import Sidebar from "../header/Sidebar";
import App from "../../App";
import renderer from 'react-test-renderer';
import React from "react";
import { MemoryRouter } from "react-router";
import "@testing-library/jest-dom/extend-expect";

afterEach(()=>{
    cleanup();
});

jest.mock("../header/Title");
jest.mock("../header/Sidebar");

test("should render title and sidebar on default", () => {
    Title.mockImplementation(() => <div>TitleMock</div>);
    Sidebar.mockImplementation(() => <div>SidebarMock</div>);
    render(
      <MemoryRouter>
        <Title />
        <App />
      </MemoryRouter>
    );
    expect(screen.getByText("TitleMock")).toBeInTheDocument();
    expect(screen.getByText("SidebarMock")).toBeInTheDocument();
});

it('renders the Title component', () => {
    const tree = renderer.create(<div>TitleMock</div>).toJSON();
    expect(tree).toMatchSnapshot();
});

it('renders the Sidebar component', () => {
    const tree = renderer.create(<div>SidebarMock</div>).toJSON();
    expect(tree).toMatchSnapshot();
});