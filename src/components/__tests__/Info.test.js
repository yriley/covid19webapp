import {render, screen, cleanup} from '@testing-library/react';
import Info from '../Info';
import coviddata from '../../data/coviddata.json';
import renderer from "react-test-renderer";

afterEach(()=>{
    cleanup();
});

test('should render Info component', () => {
    render(<Info {...coviddata} />);
    const mapStateElement = screen.getByTestId('info-test');
    expect(mapStateElement).toBeInTheDocument();
})
it('should match Info snapshot', () => {
    const InfoSnapshot = renderer.create(<Info {...coviddata} />).toJSON();
    expect(InfoSnapshot).toMatchSnapshot();
  });
  