import {render, screen, cleanup} from '@testing-library/react';
import Newsfeed from '../Newsfeed';
import renderer from "react-test-renderer";

afterEach(()=>{
    cleanup();
});

test('should render Newsfeed component', () => {
    render(<Newsfeed/>);
    const mapStateElement = screen.getByTestId('newsfeed-test');
    expect(mapStateElement).toBeInTheDocument();
})
it("should match newsfeed snapshot", () => {
    const newsFeed = renderer.create(<Newsfeed />).toJSON();
    expect(newsFeed).toMatchSnapshot();
  });
  