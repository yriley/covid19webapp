import {render, screen, cleanup} from '@testing-library/react';
import Map from '../Map';
import usmap from '../../data/States.json';
import counties from '../../data/Counties.json';
import renderer from "react-test-renderer";

afterEach(()=>{
    cleanup();
});

test('Usmap is not Counties map', () => {
    const USMap = <Map isState ={true} mapUrl={usmap} />;
    const CountyMap = <Map isState ={false} mapUrl={counties} />;
    expect(USMap).not.toMatchObject(CountyMap);
})
test('should render USMap component', () => {
    render(<Map isState ={true} mapUrl={usmap} />);
    const mapStateElement = screen.getByTestId('map-test');
    expect(mapStateElement).toBeInTheDocument();
})
test('should render CountiesMap component', () => {
    render(<Map isState ={false} mapUrl={counties} />);
    const mapCountiesElement = screen.getByTestId('map-test');
    expect(mapCountiesElement).toBeInTheDocument();
})

it('should match stateMap snapshot', () => {
    const stateMap = renderer.create(<Map isState ={true} mapUrl={usmap} />).toJSON();
    expect(stateMap).toMatchSnapshot();
  });
  
it('should match countyMap snapshot', () => {
    const countyMap = renderer.create(<Map isState ={false} mapUrl={counties} />).toJSON();
    expect(countyMap).toMatchSnapshot();
  });
  